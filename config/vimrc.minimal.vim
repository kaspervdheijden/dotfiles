set shiftround shiftwidth=4 smarttab expandtab tabstop=4 softtabstop
set listchars=tab:\ \ ,trail:\ ,nbsp:\ 
set wildmenu wildmode=longest,list,full
set background=dark nocompatible
set backspace=indent,eol,start
set autoindent smartindent
set number relativenumber
set showcmd cmdheight=1
set cursorline mouse=a
set path+=**

filetype plugin indent on
syntax on

colorscheme habamax

hi MatchParen      ctermfg=007   ctermbg=white
hi specialkey      ctermfg=233   ctermbg=210
hi statusline      ctermfg=233   ctermbg=141
hi Normal          ctermbg=NONE  guibg=NONE
hi EndOfBuffer     ctermbg=NONE
hi CursorLine      ctermbg=NONE
hi NonText         ctermbg=NONE
hi LineNr          ctermbg=NONE
