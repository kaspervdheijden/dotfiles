set nocompatible
set autoindent smartindent
set autoread
set background=dark
set backspace=indent,eol,start
set cmdheight=1
set completeopt=menuone,preview,noinsert
set display+=lastline
set diffopt+=vertical
set encoding=utf-8 fileencoding=utf-8
set ffs=unix,dos,mac
set fillchars+=vert:\ 
set foldcolumn=1
set hidden
set history=100
set hlsearch
set ignorecase
set incsearch
set langmenu=en
set lazyredraw
set linebreak
set list
set listchars=tab:\ \ ,trail:\ ,nbsp:\ 
set mat=2
set noautowrite
set nobackup
set noerrorbells
set nofoldenable
set noignorecase
set noshowmode
set nospell
set noswapfile
set noundofile
set nowrap
set nowritebackup
set number relativenumber
set numberwidth=5
set modeline
set notermguicolors
set pastetoggle=<F12>
set path+=**
set ruler
set scrolloff=8
set shiftround
set shiftwidth=4
set shm+=I
set showcmd
set showmatch
set showtabline=0
set shortmess+=cF
set smartcase
set smarttab expandtab tabstop=4 softtabstop=4
set splitbelow
set splitright
set suffixes=.bak,~,.swp,.o,.info,.tmp,.cb
set tabpagemax=8
set textwidth=0
set timeoutlen=1000 ttimeoutlen=0
set title
set ttyfast
"set t_Co=256
set t_vb=
set viminfo=""
set whichwrap+=<,>,h,l,[,]
set wildmenu
set wildmode=longest,list,full

if $MOUSE == 'off'
    set mouse=
else
    set mouse=a
endif

if has('nvim')
    set clipboard=unnamedplus
    set mousemodel=extend
endif

filetype plugin indent on
syntax on

try
    colorscheme habamax
    set cursorline
catch
    colorscheme default
endtry

try
    set ttymouse=sgr
catch
endtry

try
    set viminfofile=NONE
catch
endtry

let &t_SI                  = "\<esc>[6 q"
let &t_SR                  = "\<esc>[6 q"
let &t_EI                  = "\<esc>[2 q"
let mapleader              = ' '
let skip_defaults_vim      = 1

" Configure netrw
let g:netrw_altv           = 1
let g:netrw_banner         = 0
let g:netrw_browse_split   = 0
let g:netrw_dirhistmax     = 0
let g:netrw_hide           = 0
let g:netrw_keepdir        = 1
let g:netrw_liststyle      = 3
let g:netrw_list_hide      = netrw_gitignore#Hide()
let g:netrw_preview        = 1
let g:netrw_sizestyle      = 'H'
let g:netrw_special_syntax = 1
let g:netrw_winsize        = 25

if has('clipboard')
    vnoremap <silent> <leader>cc "+y<cr>gv
elseif executable('wl-copy')
    nnoremap <silent> <leader>cc ^v$"ky:call system('wl-copy', getreg('k'))<cr>gv
    vnoremap <silent> <leader>cc "ky:call system('wl-copy', getreg('k'))<cr>gv
elseif executable('xclip')
    nnoremap <silent> <leader>cc ^v$"ky:call system('xclip -selection clipboard', getreg('k'))<cr>gv
    vnoremap <silent> <leader>cc "ky:call system('xclip -selection clipboard', getreg('k'))<cr>gv
endif

vnoremap <silent> <leader>cf "ky:call writefile(getreg('k', 1, 1), 'vim-selection.txt')<cr>gv
xnoremap <silent> <leader>p "_dP
nnoremap <leader>r :%s/\<<C-r><C-w>\>/<C-r><C-w>/gi<Left><Left><Left>

" Quit
vnoremap <silent> <A-esc>    <esc>:qa!<cr>
inoremap <silent> <A-esc>    <esc>:qa!<cr>
nnoremap <silent> <A-esc>    :qa!<cr>
nnoremap <silent> ZX         :qa!<cr>
vnoremap <silent> ZX         :qa!<cr>

nnoremap <silent> T i    <esc>l
nnoremap <silent> t i <esc>l
nnoremap <silent> <bs> X

nnoremap J mzJ`z

" Navigate buffers
vnoremap <silent> <C-PageDown> <esc>:bprevious<cr>a
vnoremap <silent> <C-PageUp>   <esc>:bnext<cr>a
inoremap <silent> <C-PageDown>   <esc>:bprevious<cr>a
inoremap <silent> <C-PageUp>   <esc>:bnext<cr>a
nnoremap <silent> <C-PageDown> :bprevious<cr>
nnoremap <silent> <C-PageUp>   :bnext<cr>
nnoremap          <leader>\    :ls<cr>:b 
nnoremap <silent> <leader>`    :e #<cr>
nnoremap <silent> Q            :bd<cr>

nnoremap <silent> cfw ^ciw
nnoremap <silent> dfw ^diw
nnoremap <silent> dfw ^dw
nnoremap <silent> clw $ciw
nnoremap <silent> dlw $diw

vnoremap <silent> <C-g> <esc>:Lexplore<cr>
inoremap <silent> <C-g> <esc>:Lexplore<cr>
nnoremap <silent> <C-g> :Lexplore<cr>

inoremap <silent> <C-c> <esc>
vnoremap <silent> <C-c> <esc>
nnoremap <silent> <C-c> :noh<cr>
vnoremap <silent> <C-q> <esc>ggVG
inoremap <silent> <C-q> <esc>ggVG
nnoremap <silent> <C-q> ggVG
nnoremap \ :

nnoremap <leader>j  f,a<cr><esc>
vnoremap <C-p>      <esc>:find 
inoremap <C-p>      <esc>:find 
nnoremap <C-p>      :find 
nnoremap <leader>fr :%s/

" Create newlines without leaving normal mode
nnoremap <silent> gm :<C-u>call append(line('.'),   repeat([''], v:count1))<cr>
nnoremap <silent> gM :<C-u>call append(line('.')-1, repeat([''], v:count1))<cr>

" Easier switching between splits
nnoremap <silent> <leader>sr :vsplit<cr>
nnoremap <silent> <leader>sb :split<cr>
nnoremap <silent> <C-j> <C-w><C-j>
nnoremap <silent> <C-l> <C-w><C-l>
nnoremap <silent> <C-k> <C-w><C-k>
nnoremap <silent> <C-h> <C-w><C-h>

" Searching
nnoremap <silent> n nzz
nnoremap <silent> N Nzz

" Move lines
nnoremap <silent> <C-up>     :m .-2<cr>==
nnoremap <silent> <S-C-up>   :m .-2<cr>
nnoremap <silent> <C-down>   :m .+1<cr>==
nnoremap <silent> <S-C-down> :m .+1<cr>

inoremap <silent> <C-up>     <esc>:m .-2<cr>==gi
inoremap <silent> <S-C-up>   <esc>:m .-2<cr>i
inoremap <silent> <C-down>   <esc>:m .+1<cr>==gi
inoremap <silent> <S-C-down> <esc>:m .+1<cr>i

vnoremap <silent> <C-up>     :m '<-2<cr>gv=gv
vnoremap <silent> <S-C-up>   :m '<-2<cr>
vnoremap <silent> <C-down>   :m '>+1<cr>gv=gv
vnoremap <silent> <S-C-down> :m '>+1<cr>
vnoremap <silent> <S-down>   j
vnoremap <silent> <S-up>     k

" Make shift tab inverse tab
vnoremap <silent> <       <gv
vnoremap <silent> >       >gv
vmap     <silent> <tab>   >gv
vmap     <silent> <S-tab> <gv
inoremap <silent> <S-tab> <C-d>
nnoremap <silent> <tab>   >>
nnoremap <silent> <S-tab> <<

inoremap <S-up> <esc>vk
nnoremap <S-up> vk

inoremap <S-down> <esc>vj
nnoremap <S-down> vj

inoremap <C-v> <esc>l<C-v>

" Make Y yank to end of line
nnoremap <silent> Y y$
nnoremap <silent> YL ^v$hYgv
nnoremap <silent> L  ^v$h

inoremap <silent> <home> <esc>I
inoremap <silent> <end>  <esc>A
nnoremap <silent> <home> ^
nnoremap <silent> <end>  $

" Let Ctrl-Space start autocompletion
if has('nvim')
    inoremap <silent> <C-space> <C-p>
else
    inoremap <silent> <C-@> <C-p>
endif

" Remap Ctrl+E to navigate to the end of the line
noremap  <silent> <C-e> $
inoremap <silent> <C-a> <esc>I
inoremap <silent> <C-e> <esc>A
nnoremap <silent> <C-s> :w<cr>
vnoremap <silent> <C-s> <esc>:w<cr>gv
inoremap <silent> <C-s> <esc>:w<cr>a

nnoremap        <leader>m      :exec &mouse == 'a' ? 'set mouse=' : 'set mouse=a'<cr>
vnoremap        <leader>m <esc>:exec &mouse == 'a' ? 'set mouse=' : 'set mouse=a'<cr>
inoremap <expr> <right>        pumvisible() ? "<C-y>" : "<right>"
inoremap <expr> <cr>           pumvisible() ? "<C-y>" : "<cr>"

vnoremap <silent> <leader>s :!sort<cr>gv

" Allow saving files under root
command! W execute 'w !sudo tee % >/dev/null' <bar> edit!

" I just can't type invocie :shrug:
iabbrev tempalte template
iabbrev invocie  invoice


augroup commentsForFiletype
    autocmd!

    autocmd FileType php,js,java,c                    nnoremap <buffer> gc :s#^#//#<cr>:nohl<cr>
    autocmd FileType php,js,java,c                    vnoremap <buffer> gc :s#^#//#<cr>:nohl<cr>gv
    autocmd FileType php,js,java,c                    nnoremap <buffer> gC :s#^//##<cr>:nohl<cr>
    autocmd FileType php,js,java,c                    vnoremap <buffer> gC :s#^//##<cr>:nohl<cr>gv

    autocmd FileType lua,sql                          nnoremap <buffer> gc :s/^/--/<cr>:nohl<cr>
    autocmd FileType lua,sql                          vnoremap <buffer> gc :s/^/--/<cr>:nohl<cr>gv
    autocmd FileType lua,sql                          nnoremap <buffer> gC :s/^--//<cr>:nohl<cr>
    autocmd FileType lua,sql                          vnoremap <buffer> gC :s/^--//<cr>:nohl<cr>gv

    autocmd FileType sh,bash,zsh,tmux,yaml,yml,python nnoremap <buffer> gc :s/^/#/<cr>:nohl<cr>
    autocmd FileType sh,bash,zsh,tmux,yaml,yml,python vnoremap <buffer> gc :s/^/#/<cr>:nohl<cr>gv
    autocmd FileType sh,bash,zsh,tmux,yaml,yml,python nnoremap <buffer> gC :s/^#//<cr>:nohl<cr>
    autocmd FileType sh,bash,zsh,tmux,yaml,yml,python vnoremap <buffer> gC :s/^#//<cr>:nohl<cr>gv

    autocmd FileType vim                              nnoremap <buffer> gc :s/^/"/<cr>:nohl<cr>
    autocmd FileType vim                              vnoremap <buffer> gc :s/^/"/<cr>:nohl<cr>gv
    autocmd FileType vim                              nnoremap <buffer> gC :s/^"//<cr>:nohl<cr>
    autocmd FileType vim                              vnoremap <buffer> gC :s/^"//<cr>:nohl<cr>gv
augroup END

augroup netrwClose
    autocmd!
    autocmd WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&filetype") == 'netrw' | qa | endif
augroup END

augroup checkExternalModified
    autocmd!
    autocmd FocusGained,BufEnter * checktime
augroup END

augroup fileTypeDefaults
    autocmd!

    autocmd FileType htm,html,xhtml setlocal omnifunc=htmlcomplete#CompleteTags
    autocmd FileType css            setlocal omnifunc=csscomplete#CompleteCSS
    autocmd FileType yaml,yml       setlocal cursorcolumn
    autocmd FileType php            setlocal omnifunc=phpcomplete#CompletePHP
                                \ | setlocal iskeyword+=$
augroup END

augroup qKeyClosesBuffer
    autocmd!
    autocmd FileType help,netrw nmap <silent> q :bd!<cr>
augroup END

augroup toggleRelativeNumber
    autocmd!
    autocmd InsertEnter * setlocal norelativenumber
    autocmd InsertLeave * setlocal relativenumber
augroup END

augroup statusLineMode
    autocmd!

    autocmd InsertEnter * hi statusline ctermfg=237 ctermbg=39
    autocmd InsertLeave * hi statusline ctermfg=235 ctermbg=147
    autocmd BufEnter    * hi statusline ctermfg=235 ctermbg=147
augroup END

augroup removeTrailingWhiteSpace
    autocmd!
    autocmd BufWritePre *.js,*.html,*.py,*.sh,*.php,*.java :call RemoveTrailingWhiteSpace()
augroup END

augroup startInInsertModeForEmptyBuffers
    autocmd!

    autocmd BufReadPost * if getfsize(expand('%')) == 0 | startinsert | endif
    autocmd VimEnter    * if empty(expand('%'))         | startinsert | endif
    autocmd BufNewFile  * startinsert
augroup END

function! RemoveTrailingWhiteSpace()
    let save_cursor = getpos('.')
    let old_query   = getreg('/')

    silent! %s/\s\+$//e

    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfunction

hi MatchParen      ctermfg=007   ctermbg=white
hi specialkey      ctermfg=233   ctermbg=210
hi statusline      ctermfg=233   ctermbg=141
hi User1           ctermfg=007   ctermbg=none cterm=none
hi User2           ctermfg=007   ctermbg=none cterm=none
hi User3           ctermfg=236   ctermbg=236
hi User4           ctermfg=235   ctermbg=green

hi EndOfBuffer     ctermbg=NONE
hi CursorLine      ctermbg=NONE
hi NonText         ctermbg=NONE
hi Normal          ctermbg=NONE
hi LineNr          ctermbg=NONE
hi ExtraWhitespace ctermbg=red

match ExtraWhitespace /\s\+$/

if has('statusline')
    set laststatus=2
    set statusline  =%0*\ \ %{GetCurrentMode()}\ \          " The current mode
    set statusline +=%4*%{StatusLineGit()}                  " Branch name
    set statusline +=%5*\ \ %n\ \                           " Buffer number
    set statusline +=%2*\ %Y                                " FileType
    set statusline +=%1*\ %<%F%m%r%h%w\ \                   " File path, modified, readonly, helpfile, preview
    set statusline +=%=                                     " Right side
    set statusline +=%2*\ %{''.(&fenc!=''?&fenc:&enc).''}\  " Encoding
    set statusline +=\ %{&ff}                               " FileFormat (dos/unix..)
    set statusline +=\ %3p%%\                               " Percentage of document
    set statusline +=%2*\ %2l:%2v\                          " Column number
    set statusline +=%0*%{&mouse=='a'?'\ mouse\ ':''}       " Mouse enabled?
    set statusline +=%0*%{&paste?'\ \ paste\ ':''}          " Paste enabled?

    let g:currentmode = {
    \     'n'      : 'NORMAL',
    \     'no'     : 'NORMAL-OPERATOR PENDING',
    \     'v'      : 'VISUAL',
    \     'V'      : 'V-LINE',
    \     "\<C-v>" : 'V-BLOCK',
    \     's'      : 'SELECT',
    \     'S'      : 'S-LINE',
    \     "\<C-s>" : 'S-BLOCK',
    \     'i'      : 'INSERT',
    \     'R'      : 'REPLACE',
    \     'Rv'     : 'V-REPLACE',
    \     'c'      : 'COMMAND',
    \     'cv'     : 'VIM EX',
    \     'ce'     : 'EX',
    \     'r'      : 'PROMPT',
    \     'rm'     : 'MORE',
    \     'r?'     : 'CONFIRM',
    \     '!'      : 'SHELL',
    \     't'      : 'TERMINAL',
    \ }

    function! GetCurrentMode()
        return get(g:currentmode, mode(), mode())
    endfunction

    " Returns current git branch name, if applicable
    function! StatusLineGit()
        :silent! let l:branchname = system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
        return strlen(l:branchname) > 0 ? '   ' . l:branchname . '   ' : ''
    endfunction
endif
