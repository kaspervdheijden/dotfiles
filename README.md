## Dotfiles

This repository enables me to quickly setup a shell environment to my liking, and uses the following binaries:

1. git (*required*)
2. fzf
3. htop
4. ripgrep
5. shellcheck
6. tmux
7. tree
8. zsh

Install all:
```sh
"${DF_ROOT_DIR}/setup/install.sh deps"

# or just
sdf deps
```

### Neovim
Currently, this repository ships an old-style (vimscript) vimrc without any plugins. This is used for simple file edits and
lightweight shell script coding, but for serious development use the `Ctrl-/` keybind in tmux to switch to a dedicated repository
environment window. This will install neovim as an appimage, and install my [neovim](https://gitlab.com/Kaspervdheijden/kickstart-config.git)
configs.

### Installation

First, clone this repository, call `install.sh`, and reload your shell.
```sh
git clone 'https://gitlab.com/Kaspervdheijden/dotfiles.git'
./dotfiles/setup/install.sh
```

When using bash, you can make autocomplete case-insensitive by doing:
```sh
[ ! -f ~/.inputrc ] && [ -f /etc/.inputrc ] && printf '$include /etc/.inputrc\n' > ~/.inputrc
printf 'set completion-ignore-case On\n' >> ~/.inputrc
```

### Notes

#### gitc function (toolgit commit script)
Working with a repository where you don't need (or want) to perform fork-, default branch- and/or
newline checks, you can pass the appropriate flags or define git variables for them.

Add the variable to the specific repository, to not have to pass `-f` anymore:
```sh
# override line endings check, to not have to pass -n
git config --local dotfiles.checkLineEndings 0

# override fork check, to not have to pass -f
git config --local dotfiles.checkFork 0

# override default branch check, to not have to pass -d
git config --local dotfiles.checkDefaultBranch 0
```

This needs to be done on every machine, since git variables aren't part of the repository itself.

You can also use global variables, `$DF_CHECK_FORK`, `$DF_CHECK_LINE_ENDINGS`
and `$DF_CHECK_DEFAULT_BRANCH`. If set in variables, they apply to _all_ repositories.

#### Prompt
The `$DF_PROMPT_GIT_STATUS` controls the git status information in the prompt.
- n: Do not show anything
- a: Show all status
- c: Show only changes files (no untracked files)

The default is `c`. Setting it to `a` may significantly slow down the prompt on older repos.

You can define the `$DF_DOTFILES_HOST_PROMPT_COLOR_BASH` or `$DF_DOTFILES_HOST_PROMPT_COLOR_ZSH`
variables to control the color for the host part in the prompt. It defaults to green.

### Plugins
Dotfiles supports custom plugins. These are git repositories from which a shell script will be sourced.
By default, plugins will be installed in the `./plugins` directory, but this can be overriden
by the `$DF_PLUGIN_DIR` variable.
