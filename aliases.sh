#!/usr/bin/env false
# shellcheck shell=sh disable=SC1008

alias grep='grep --color=auto --exclude-dir=vendor --exclude-dir=.git --exclude-dir=.composer --exclude-dir=.idea --exclude-dir=build'
#alias dff='diff --suppress-common-lines --side-by-side --ignore-tab-expansion'
alias vm='${EDITOR} -u "${DF_ROOT_DIR}/config/vimrc.minimal.vim"'
alias lsa='ls -lAFhH --group-directories-first --color=auto'
alias pre='vide --profile=personal'
alias ls='ls --color=auto'
alias cl='cd && clear'
alias v='${EDITOR}'

alias gh='toolgit commit -aE >/dev/null && { for x in 1 2 3 4 5; do toolgit push -f && break; done || printf "No luck after 5 attempts :tableflip:\n"; }'
alias rr='cd "$(git rev-parse --show-toplevel 2>/dev/null)"'
alias gcl='toolgit unlock && git checkout -- .'
alias grp='toolgit unlock && git reset -p'
alias gap='toolgit unlock && git add -p'
alias gds='git diff --staged'
alias g1='toolgit commits 1'
alias g2='toolgit commits 2'
alias g3='toolgit commits 3'
alias gtv='git remote -v'
alias gs='toolgit status'
alias gd='git diff'
alias g='git'

alias gcml='toolgit unlock && toolgit branch checkoutmain && toolgit pull && toolgit commits'
alias gitl='toolgit unlock && toolgit pull && toolgit commits'
alias gcr='toolgit unlock && toolgit branch checkoutremote'
alias gcm='toolgit unlock && toolgit branch checkoutmain'
alias mainin='toolgit unlock && toolgit branch mainin'
alias gre='toolgit unlock && toolgit branch checkout'
alias gbd='toolgit unlock && toolgit branch delete'
alias gitb='toolgit unlock && toolgit branch new'
alias gitc='toolgit unlock && toolgit commit'
alias gith='toolgit unlock && toolgit push'
alias gf='toolgit unlock && toolgit fetch'
alias gc='toolgit unlock && git checkout'
alias ga='toolgit unlock && git add'
alias gb='toolgit branch list'
