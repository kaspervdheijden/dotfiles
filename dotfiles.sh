#!/usr/bin/env false
# shellcheck shell=sh

# shellcheck source=./functions.sh
. "${DF_ROOT_DIR}/functions.sh"

# shellcheck source=./aliases.sh
. "${DF_ROOT_DIR}/aliases.sh"

if [ -f "${XDG_CONFIG_HOME:-${HOME}/.config}/dotfiles-local-config/dotfiles.sh" ]; then
    # shellcheck source=/dev/null
    . "${XDG_CONFIG_HOME:-${HOME}/.config}/dotfiles-local-config/dotfiles.sh"
fi

if [ -f "${XDG_CONFIG_HOME:-${HOME}/.config}/dotfiles-local-config/ssh-aliases.sh" ]; then
    # shellcheck source=/dev/null
    . "${XDG_CONFIG_HOME:-${HOME}/.config}/dotfiles-local-config/ssh-aliases.sh"
fi

#
# Conditionals
#

if ! command -v shellcheck >/dev/null && command -v docker >/dev/null; then
    alias shellcheck='docker container run --rm --mount type=bind,source="$(pwd)",target=/share,readonly docker.io/polymathrobotics/shellcheck --color=always --norc --external-sources'
fi

if command -v mutt >/dev/null && [ -x "${XDG_CONFIG_HOME:-${HOME}/.config}/mutt/start-mutt" ]; then
    # shellcheck disable=SC2139
    alias mtt="${XDG_CONFIG_HOME:-${HOME}/.config}/mutt/start-mutt"
fi

if command -v php >/dev/null; then
    if command -v composer >/dev/null; then
        alias roave='composer require --dev --no-install --dry-run roave/security-advisories:dev-latest'
        alias ci='composer install'
    fi

    run_vendor_bin() {
        (
            utility="${1}"
            if [ -z "${utility}" ]; then
                printf 'No utitily given\n' >&2
                exit 11
            fi

            repo_root="$(git rev-parse --show-toplevel 2>/dev/null)"
            if [ -z "${repo_root}" ]; then
                repo_root='.'
            fi

            if [ ! -f "${repo_root}/vendor/bin/${utility}" ]; then
                printf 'Could not find "%s"!\n' "${utility}" >&2
                exit 12
            fi

            printf '%s/vendor/bin/%s %s\n' "${repo_root}" "${utility}" "${2}"
            env PHP_CS_FIXER_IGNORE_ENV=1 sh -c "${repo_root}/vendor/bin/${utility} ${2}"
        )
    }

    alias phpunit="   run_vendor_bin 'phpunit'      '--coverage-text --testdox -d memory_limit=2G'"
    alias rectorcc="  run_vendor_bin 'rector'       'process --memory-limit=2G --clear-cache'"
    alias phpstan="   run_vendor_bin 'phpstan'      'analyse --memory-limit=2G'"
    alias rector="    run_vendor_bin 'rector'       'process --memory-limit=2G'"
    alias phpcsfixer="run_vendor_bin 'php-cs-fixer' 'fix -v'"
    alias phpcs="     run_vendor_bin 'phpcs'"
fi

if command -v speedtest-cli >/dev/null; then
    alias st='speedtest-cli --secure --no-upload'
    alias stu='speedtest-cli --secure'
fi

if command -v xdg-open >/dev/null; then
    alias x='xdg-open'
fi
