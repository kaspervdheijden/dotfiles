##!/usr/bin/env false
# shellcheck shell=sh
#
# dep=ripgrep,tree,tmux,zsh,fzf

#
# Changes to the directory, and lists its contents
# If no directory given, changes to the home dir
#
# cdc [<directory>]
#
cdc() {
    cd "${@}" && ls --color=auto -lAFhH --color=always --group-directories-first
}

#
# Navigates to a repository given a parial name
#
# cds [<parital-name>]
#
cds() {
    if [ -z "${1}" ]; then
        repo-list
    elif [ "${1}" = '-' ]; then
        cd - || return 11
    elif [ -d "${1}/.git" ]; then
        cd "${1}" || return 12
    else
        cd "$( \
            repo_list="$(repo-list)"
            search="${1}"

            matches="$(printf '%s\n' "${repo_list}" | grep -Eie "/[^/]*${search}[^/]*$")"
            if [ -z "${matches}" ]; then
                printf 'No matches found for "%s*"\n' "${search}" >&2
                return 1
            fi

            if [ -d "${matches}" ]; then
                printf '%s\n' "${matches}"
                return
            fi

            matches="$(printf '%s\n' "${repo_list}" | grep -Eie "/[^/]*${search}$")"
            if [ -z "${matches}" ]; then
                printf 'No matches found for "*%s$"\n' "${search}" >&2
                return 1
            fi

            if  [ -d "${matches}" ]; then
                printf '%s\n' "${matches}"
                return
            fi

            match="$(printf '%s\n' "${repo_list}" | grep -Eie "/${search}$")"
            if  [ -d "${match}" ]; then
                printf '%s\n' "${match}"
                return
            fi

            printf 'Multiple matches found:\n%s\n' "${matches}" >&2
            return 1
        )" || return 13
    fi
}

#
# Creates shell script
#
# - Creates any non existant subdirs
# - Adds shebang to empty files
# - Makes file executable
# - Appends STDIN
# - Edits file in $EDITOR
#
# mkscript <file-name>
#
mkscript() {
    if [ -z "${1}" ]; then
        printf 'No name given\n' >&2
        return 11
    fi

    while [ -n "${1}" ]; do
        if ! mkdir -p "$(dirname "${1}")"; then
            printf 'Could not create the directory for "%s"\n' "${1}" >&2
            return 12
        fi

        if [ ! -s "${1}" ]; then
            printf '#!/usr/bin/env sh\n\n\n' > "${1}"
        fi

        if [ ! -x "${1}" ]; then
            chmod +x "${1}"
        fi

        if [ ! -t 0 ]; then
            cat ->>"${1}"
        fi

        if printf '%s' "${EDITOR}" | grep -qE '^n?vim?$'; then
            "${EDITOR}" -c 'normal!G' -c 'startinsert' "${1}"
        else
            "${EDITOR}" "${1}"
        fi

        shift
    done
}

#
# Manage dotfiles
#
# sdf <update|install|nav|deps|reload>
#
# update [<remote>]  Updates dotfiles
# install            (Re)installs dotfiles
# nav                Navigates to the dotfiles repo
# deps               Shows dependencies
# reload             Reloads configuration
#
sdf() {
    # shellcheck disable=SC2164
    case "${1:-nav}" in
        update)
            if "${DF_ROOT_DIR}/setup/install.sh" update "${2}"; then
                sdf reload
            fi
            ;;
        install) "${DF_ROOT_DIR}/setup/install.sh" install "${2}"; sdf reload ;;
        nav)     toolgit check "${DF_ROOT_DIR}" && cd "${DF_ROOT_DIR}"        ;;
        deps)    "${DF_ROOT_DIR}/setup/install.sh" dependencies               ;;
        reload)
            if [ -n "${TMUX}" ]; then
                tmux source-file "${DF_ROOT_DIR}/config/tmux.conf"
                tmux display -pt1 'tmux configuration reloaded!'
            fi

            exec "$(ps -p "${$}" -o args= | sed 's/^-//')"
            ;;
        *)
            printf 'Command not recognized: "%s"\n' "${1}" >&2
            return 11
            ;;
    esac
}

#
# Shows the source of a shell function, alias or script
#
# show <function>
#
show() {
    (
        if [ -z "${1}" ]; then
            return
        fi

        tpe="$(type "${1}" 2>/dev/null | sed 's/ is aliased to /=/; s/ is an alias for /=/; s/ a shell function/ a function/' | head -n1)"
        if [ -z "${tpe}" ] || printf '%s\n' "${tpe}" | grep -qE 'not found$'; then
            printf '"%s" is undefined\n' "${1}" >&2
            return 1
        elif printf '%s\n' "${tpe}" | grep -qF ' is a function'; then
            # shellcheck disable=SC3044
            declare -f "${1}"
        else
            printf '%s\n' "${tpe}"

            exec_path="$(command -v "${1}" 2>/dev/null)"
            if file "${exec_path}" 2>/dev/null | grep -qF 'script, ASCII text'; then
                less "${exec_path}" 2>/dev/null
            fi
        fi
    )
}

#
# Creates directories and changes directory into the last directory given
#
# take <directory1> <directory2> ...<directoryN>
#
take() {
    while [ -n "${1}" ]; do
        if printf '%s' "${1}" | grep -qE  '^git@|https?://'; then
            if ! git clone "${1}"; then
                printf 'Could not clone repository "%s"\n' "${1}" >&2
                return 11
            fi

            if ! cd "$(printf '%s' "${1##*/}" | sed 's/\.git$//')"; then
                printf 'Could not enter "%s"\n' "$(printf '%s' "${1##*/}" | sed 's/\.git$//')" >&2
                return 12
            fi
        else
            if ! mkdir -p "${1}"; then
                printf 'Could not create "%s"\n' "${1}" >&2
                return 11
            fi

            if ! cd "${1}"; then
                printf 'Could not enter "%s"\n' "${1}" >&2
                return 12
            fi
        fi

        shift
    done
}

#
# Starts or (re-)attaches a tmux session
#
tm() {
    if ! command -v tmux >/dev/null; then
        printf 'Binary not found: tmux\n' >&2
        return 127
    fi

    (
        name='default'
        mouse=''

        for arg; do
            case "${arg}" in
                '-M' | '--no-mouse') mouse='off' ;;
                *)
                    if [ -n "${arg}" ]; then
                        name="${arg}"
                    fi

                    ;;
            esac
        done

        env MOUSE="${mouse}" tmux -f "${DF_ROOT_DIR}/config/tmux.conf" -2u new-session -As "${name}"
    )
}

update() {
    if [ "${1}" != '-f' ] && command -v systemctl >/dev/null && systemctl is-active --quiet puppet.service >/dev/null; then
        printf 'Not updating because this system is under puppet control\n' >&2
        return 11
    fi

    if command -v flatpak >/dev/null && [ -z "$(flatpak list | wc -l)" ]; then
        flatpak update --assumeyes
    fi

    printf 'Running kernel: \033[0;33m%s\033[0;0m\n' "$(uname -r)"
    case "$({ command -v pacman || command -v apt || command -v dnf; } | sed 's#.\+/##g')" in
        apt)    sudo sh -c 'apt update -y && apt upgrade -y && apt autoremove -y && apt autoclean -y' ;;
        dnf)    sudo sh -c 'dnf -y update && dnf -y autoremove'                                       ;;
        pacman) sudo pacman -Syyu --noconfirm                                                         ;;
        *)      printf 'Could not determine your package manager\n' >&2; return 12                    ;;
    esac
}
