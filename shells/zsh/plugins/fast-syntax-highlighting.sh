#!/usr/bin/env sh

# shellcheck disable=SC1090

. "${1}"

fast-theme -q "${DF_ROOT_DIR}/shells/zsh/plugins/overlay"
