#!/usr/bin/env sh

# shellcheck disable=SC1090
. "${1}"

export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=244"
