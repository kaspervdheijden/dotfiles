#!/usr/bin/env false
# shellcheck shell=bash

__df_prompt_git_status() {
    local branch_name

    branch_name="$(git symbolic-ref --short HEAD 2>/dev/null)"
    [ -z "${branch_name}" ] && return

    local output

    output="%F{green}%B${branch_name}%b%f"
    case "${DF_PROMPT_GIT_STATUS:-c}" in
        c) [ -n "$(git status -uno --porcelain 2>/dev/null)" ] && output="${output}*" ;;
        a) [ -n "$(git status --porcelain 2>/dev/null)" ]      && output="${output}*" ;;
    esac

    printf ' %s' "${output}"
}

__df_prompt_host_color() {
    printf '%s' "${DF_HOST_PROMPT_COLOR_ZSH:-%F{green}}"
}

__os_prompt="$(uname -s) $(uname -r | grep -Eo '^[0-9]+.[0-9]+.[0-9]+')"
PS1='[%(?..%B%F{red}%?%f%b )%B%F{white}%n%f%b@%f$(__df_prompt_host_color)%U%M%u%f %F{yellow}zsh%f $__os_prompt %F{cyan}%~%f %D{%Y-%m-%d} %T$(__df_prompt_git_status)]
%(!.#.%%) '
