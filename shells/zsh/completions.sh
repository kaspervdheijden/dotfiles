#!/usr/bin/env false
# shellcheck shell=bash

zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
# shellcheck disable=SC2016
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
zstyle ':completion:*' completer _expand _complete _correct _approximate
# shellcheck disable=SC2296
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}" ma=0\;33
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=5
zstyle ':completion:*' verbose true

compdef _remotes gith gitl gf
compdef _unstaged_files gt
compdef _staged_files gtf
compdef _branches gc gbd
compdef _show show
compdef _pre pre
compdef _cds cds
compdef _sdf sdf
compdef cdc=cd

_show() {
    _arguments "1: :($({ declare -f | awk '/^[a-zA-Z][a-zA-Z0-9_-]+ \(\) {?$/ {print $1}'; alias | cut -d'=' -f1; } | sort | uniq | xargs))"
}

_branches() {
    _arguments "1: :($(git branch 2>/dev/null| grep -vE '^\* ' | cut -c3-))"
}

_remotes() {
    _arguments "1: :($(git remote 2>/dev/null))"
}

_pre() {
    if [ "${DF_CDS_COMPLETE_FULL_PATHS:-0}" -eq 0 ]; then
        _arguments "1: :($(DF_CDS_REPO_DIRS="${XDG_CONFIG_HOME:-${HOME}/.config}" DF_CDS_MAX_DEPTH=4 repo-list | xargs -I {} basename {} | xargs))"
    else
        _arguments "1: :($(DF_CDS_REPO_DIRS="${XDG_CONFIG_HOME:-${HOME}/.config}" DF_CDS_MAX_DEPTH=4 repo-list | xargs))"
    fi
}

_cds() {
    if [ "${DF_CDS_COMPLETE_FULL_PATHS:-0}" -eq 0 ]; then
        _arguments "1: :($(repo-list | xargs -I {} basename {} | xargs))"
    else
        _arguments "1: :($(repo-list | xargs))"
    fi
}

_sdf() {
    # shellcheck disable=SC2016
    _arguments '1: :($(declare -f sdf | grep -Eo "^		\([a-zA-Z0-9_-]+\)" | tr -d "()" | xargs))'
}

_staged_files() {
    _arguments "1: :$(git diff --name-only --cached)"
}

_unstaged_files() {
    _arguments "1: :$(git diff --name-only)"
}
