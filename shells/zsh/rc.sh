#!/usr/bin/env false
# shellcheck shell=bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

autoload -U compinit && compinit -D
# autoload -U colors   && colors

bindkey -e
bindkey '^R'      history-incremental-search-backward # Make Ctrl+R be incremental
bindkey '^p'      history-search-backward             # Ctrl+p:         history reverse search
bindkey '^n'      history-search-forward              # Ctrl+n:         history forward search
bindkey '^[[Z'    reverse-menu-complete               # Shift+Tab:      move through completion menu backwards
bindkey '^H'      backward-delete-word                # Ctrl+Backspace: delete word backwards
bindkey '^[[H'    beginning-of-line                   # HOME key:       to beginning of line
bindkey '^[[1;5D' backward-word                       # Ctrl+Left:      move word backwards
bindkey '^[[1;5C' forward-word                        # Ctrl+Right:     move word forwards
bindkey '^[[3;5~' kill-word                           # Ctrl+Del:       delete word forwards
bindkey '^[[3~'   delete-char                         # DELETE key:     delete char
bindkey '^[[F'    end-of-line                         # END key:        to end of line

unsetopt menu_complete

setopt hist_expire_dups_first
setopt hist_ignore_all_dups
setopt interactivecomments
setopt inc_append_history
setopt hist_reduce_blanks
setopt hist_ignore_space
setopt hist_find_no_dups
setopt hist_save_no_dups
setopt complete_in_word
setopt hist_ignore_dups
setopt auto_param_slash
setopt complete_aliases
setopt histignoredups
setopt always_to_end
setopt appendhistory
setopt sharehistory
setopt prompt_subst
setopt hist_verify
setopt auto_list
setopt auto_menu

export DF_ROOT_DIR
export HISTFILE
export HISTSIZE
export SAVEHIST
export HISTDUP
export SHELL

HISTFILE="${XDG_STATE_HOME:-${HOME}/.local/state}/zsh_history"
DF_ROOT_DIR="$(realpath "$(dirname "${0}")/../../")"
HISTCONTROL=ignoreboth
SHELL="$(which zsh)"
HISTSIZE=50000
SAVEHIST=50000
HISTDUP=erase

. "${DF_ROOT_DIR}/common.sh"
. "${DF_ROOT_DIR}/shells/zsh/completions.sh"
. "${DF_ROOT_DIR}/shells/zsh/prompt.sh"
. "${DF_ROOT_DIR}/dotfiles.sh"

unfunction command_not_found_handler 2>/dev/null
if [ -f "${DF_ROOT_DIR}/plugins/zsh.sh" ]; then
    # shellcheck source=/dev/null
    . "${DF_ROOT_DIR}/plugins/zsh.sh"
fi
