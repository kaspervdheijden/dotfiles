#!/usr/bin/env false
# shellcheck shell=bash

complete -F _remotes gitl gith gf
complete -F _unstaged_files gt
complete -F _staged_files gtf
complete -F _branches gc gbd
complete -F _show show
complete -F _pre pre
complete -F _cds cds
complete -F _sdf sdf
complete -d cdc

_pre() {
    if [ "${DF_CDS_COMPLETE_FULL_PATHS:-0}" -eq 0 ]; then
        # shellcheck disable=SC2207
        COMPREPLY=($(compgen -W "$(pre | xargs -I{} basename {} | xargs)" -- "${COMP_WORDS[$COMP_CWORD]}"))
    else
        # shellcheck disable=SC2207
        COMPREPLY=($(compgen -W "$(pre | xargs)" -- "${COMP_WORDS[$COMP_CWORD]}"))
    fi
}

_sdf() {
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$(declare -f sdf | grep -Eo '^        [a-zA-Z0-9_-]+\)' | tr -d '()' | xargs)" -- "${COMP_WORDS[$COMP_CWORD]}"))
}

_cds() {
    if [ "${DF_CDS_COMPLETE_FULL_PATHS:-0}" -eq 0 ]; then
        # shellcheck disable=SC2207
        COMPREPLY=($(compgen -W "$(repo-list | xargs -I{} basename {} | xargs)" -- "${COMP_WORDS[$COMP_CWORD]}"))
    else
        # shellcheck disable=SC2207
        COMPREPLY=($(compgen -W "$(repo-list | xargs)" -- "${COMP_WORDS[$COMP_CWORD]}"))
    fi
}

_branches() {
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$(git branch 2>/dev/null | grep -vE '^\* ' | cut -c3-)" -- "${COMP_WORDS[$COMP_CWORD]}"))
}

_remotes() {
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$(git remote 2>/dev/null)" -- "${COMP_WORDS[$COMP_CWORD]}"))
}

_show() {
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$({ declare -f | awk '/^[a-zA-Z][a-zA-Z0-9_-]+ \(\) {?$/ {print $1}'; alias | cut -d'=' -f1; } | sort | uniq | xargs)" -- "${COMP_WORDS[$COMP_CWORD]}"))
}

_unstaged_files() {
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$(git diff --name-only)" -- "${COMP_WORDS[$COMP_CWORD]}"))
}

_staged_files() {
    # shellcheck disable=SC2207
    COMPREPLY=($(compgen -W "$(git diff --name-only --cached)" -- "${COMP_WORDS[$COMP_CWORD]}"))
}
