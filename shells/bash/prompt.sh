#!/usr/bin/env false
# shellcheck shell=bash

__df_prompt_last_exitcode() {
    local latest_exitcode="${?}"
    [ "${latest_exitcode}" -eq 0 ] && return
    printf "\033[1;31m%s\033[0;00m " "${latest_exitcode}"
}

__df_prompt_git_status() {
    local output

    output="$(git symbolic-ref --short HEAD 2>/dev/null)"
    [ -z "${output}" ] && return

    case "${DF_PROMPT_GIT_STATUS:-c}" in
        c) [ -n "$(git status -uno --porcelain 2>/dev/null)" ] && output="${output}*" ;;
        a) [ -n "$(git status --porcelain 2>/dev/null)" ]      && output="${output}*" ;;
    esac

    printf ' %s' "${output}"
}

__df_prompt_char() {
    if [ "$(id -u)" -eq 0 ]; then
        printf '#'
    else
        printf '%%'
    fi
}

__df_prompt_host_color() {
    printf '%s' "${DF_HOST_PROMPT_COLOR_BASH:-4;32}"
}

__os_prompt="$(uname -s) $(uname -r | grep -Eo '^[0-9]+.[0-9]+.[0-9]+')"

# shellcheck disable=SC2025
PS1="[\$(__df_prompt_last_exitcode)\e[1;37m\u\e[0;37m@\e[$(__df_prompt_host_color)m\H\e[0m \e[0;33mbash\e[0m $__os_prompt\e[0;36m \w\e[0m \$(date +'%Y-%m-%d %H:%M')\e[1;32m\$(__df_prompt_git_status)\e[0;0m]\n$(__df_prompt_char) "
