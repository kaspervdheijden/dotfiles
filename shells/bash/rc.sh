#!/usr/bin/env false
# shellcheck shell=bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export HISTTIMEFORMAT
export HISTFILESIZE
export DF_ROOT_DIR
export HISTCONTROL
export HISTSIZE
export SHELL

# shellcheck disable=SC2128
DF_ROOT_DIR="$(realpath "$(dirname "${BASH_SOURCE}")/../..")"
HISTTIMEFORMAT='%F %T '
HISTCONTROL=ignoreboth
SHELL="$(which bash)"
HISTFILESIZE=20000
HISTSIZE=20000

shopt -s checkwinsize histappend dirspell 2>/dev/null

bind 'set completion-ignore-case on'
bind 'set show-all-if-ambiguous on'

. "${DF_ROOT_DIR}/common.sh"
. "${DF_ROOT_DIR}/shells/bash/completions.sh"
. "${DF_ROOT_DIR}/shells/bash/prompt.sh"
. "${DF_ROOT_DIR}/dotfiles.sh"

if [ -f "${DF_ROOT_DIR}/plugins/bash.sh" ]; then
    # shellcheck source=/dev/null
    . "${DF_ROOT_DIR}/plugins/bash.sh"
fi
