#!/usr/bin/env sh

install() {
    export DF_ROOT_DIR
    DF_ROOT_DIR="$(git rev-parse --show-toplevel 2>/dev/null)"

    printf 'configuring dotfiles from %s...\n' "${DF_ROOT_DIR}"
    find "${DF_ROOT_DIR}/shells" -maxdepth 2 -type f -name 'rc.sh' | while read -r file_name; do
        shell_name="$(basename "$(dirname "${file_name}")")"
        file=".${shell_name}rc"

        if [ ! -f "${HOME}/${file}" ]; then
            if [ "${verbose}" -gt 0 ]; then
                printf '  -> skip config for %s: ~/%s does not exist\n' "${shell_name}" "${file}"
            fi

            continue
        fi

        if grep -Eq "^. .*/dotfiles/shells/[a-z]+/rc.sh.$" "${HOME}/${file}" 2>/dev/null; then
            if [ "${verbose}" -gt 0 ]; then
                printf '  -> skip config for %s: already present in ~/%s\n' "${shell_name}" "${file}"
            fi

            continue
        fi

        printf '  -> adding config for %s to ~/%s\n' "${shell_name}" "${file}"
        printf '\n. "%s/shells/%s/rc.sh"\n' "${DF_ROOT_DIR}" "${shell_name}" >>"${HOME}/${file}"
    done

    printf 'copying dotfiles...\n'
    sed '/^#/d; /^$/d' "${DF_ROOT_DIR}/setup/dotfiles.conf" | while read -r source destination required_binary; do
        if ! command -v "${required_binary:-true}" >/dev/null; then
            if [ "${verbose}" -gt 0 ]; then
                printf '  -> skip %s because %s is not installed\n' "${source}" "${required_binary}"
            fi

            continue
        fi

        path_name="$(dirname "${HOME}/${destination}")"
        if [ ! -d "${path_name}" ] && ! mkdir -p "${path_name}"; then
            printf 'Could not create %s\n' "${path_name}" >&2
            continue
        fi

        printf '  -> %s => ~/%s\n' "${source}" "${destination}"
        cp "${DF_ROOT_DIR}/${source}" "${HOME}/${destination}"
    done

    if [ -f "${DF_SSH_CONF_FILE:-${XDG_CONFIG_HOME:-${HOME}/.config}/dotfiles-local-config/ssh-hosts.conf}" ]; then
        dir="${XDG_CONFIG_HOME:-${HOME}/.config}/dotfiles-local-config"

        if ! mkdir -p "${dir}"; then
            printf 'Could not create dir "%s"\n' "${dir}" >&2
        else
            printf 'creating ssh aliases...\n'
            "${DF_ROOT_DIR}/scripts/ssh-mgr" 'aliases' >"${dir}/ssh-aliases.sh"
        fi
    fi
}

update() {
    old_commit="$(git rev-parse --verify HEAD)"
    git pull --quiet --rebase "${remote}" || return 16

    new_commit="$(git rev-parse --verify HEAD)"
    if [ "${old_commit}" = "${new_commit}" ]; then
        printf '  -> dotfiles already up to date 👍\n'
        return 17
    fi

    printf '  -> updated to %s\n\ndiffstat:\n' "${new_commit}"
    git diff --stat "${old_commit}"..HEAD

    printf '\nchangelog:\n'
    git log --format="  -> %s" --no-merges "${old_commit}"..HEAD && printf '\n'
}

plugins() {
    filename="${XDG_CONFIG_HOME:-${HOME}/.config}/dotfiles/plugins.conf"
    if [ ! -f "${filename}" ]; then
        filename="${DF_ROOT_DIR}/setup/plugins.conf"

        if [ ! -f "${filename}" ]; then
            printf 'Could not load plugin files\n' >&2
            return 17
        fi
    fi

    printf 'checking for connectivity...\n'
    for domain in $(awk '! /#/ {print $4}' "${filename}" | cut -d/ -f3 | sort | uniq); do
        printf '  -> %s...' "${domain}"

        if ping -q -c1 "${domain}" >/dev/null 2>/dev/null; then
            printf ' OK\n'
        else
            printf '  ERR: Cannot connect to %s\n' "${domain}" >&2
            return 18
        fi
    done

    plugin_dir="${DF_PLUGIN_DIR:-${DF_ROOT_DIR}/plugins}"

    rm "${plugin_dir}"/*.sh >/dev/null 2>/dev/null
    printf 'configuring plugins...\n'

    sed '/^#/d; /^$/d' "${filename}" | while read -r intended_shell plugin_type plugin_name remote_uri file_to_source file_to_source_override; do
        if [ -z "${plugin_name}" ]; then
            printf 'plugin name not set\n' >&2
            continue
        fi

        if [ "${plugin_type}" != 'git' ]; then
            printf '  -> plugin type "%s" not supported for %s\n' "${plugin_type}" "${plugin_name}" >&2
            continue
        fi

        if [ -d "${plugin_dir}/${plugin_name}" ]; then
            printf '  -> updating %s\n' "${plugin_name}"

            if ! (cd "${plugin_dir}/${plugin_name}" && [ "$(git remote -v | awk '/(fetch)/ {print $2}')" = "${remote_uri}" ] && git pull -q --ff-only); then
                printf 'Could not update %s from %s\n' "${plugin_name}" "${remote_uri}" >&2
            fi
        else
            printf '  -> installing %s\n' "${plugin_name}"

            if ! git clone -q "${remote_uri}" "${plugin_dir}/${plugin_name}"; then
                printf 'Could not clone %s from %s\n' "${plugin_name}" "${remote_uri}" >&2
                continue
            fi
        fi

        [ ! -f  "${DF_ROOT_DIR}/plugins/${intended_shell}.sh" ] && printf '#!/usr/bin/env false\n# shellcheck shell=sh\n\n' > "${DF_ROOT_DIR}/plugins/${intended_shell}.sh"

        if [ -n "${file_to_source_override}" ]; then
            printf '. "%s/%s" "%s/%s/%s"\n' "${DF_ROOT_DIR}" "${file_to_source_override}" "${plugin_dir}" "${plugin_name}" "${file_to_source}" >> "${DF_ROOT_DIR}/plugins/${intended_shell}.sh"
        else
            printf '. "%s/%s/%s"\n' "${plugin_dir}" "${plugin_name}" "${file_to_source}" >> "${DF_ROOT_DIR}/plugins/${intended_shell}.sh"
        fi
    done

    cd "${plugin_dir}" || return 18
    find . -maxdepth 1 -type d -not -name '.' | cut -c3- | while read -r dir; do
        [ ! -d "./${dir}" ] && continue

        if ! grep -qF "/${dir}/" ./*.sh; then
            print '  -> purging %s\n' "${dir}"
            rm -rf "./${dir}"
        fi
    done
}


cd "$(realpath "$(dirname "${0}")/..")" || {
    printf 'Could not enter root directory\n' >&2
    exit 1
}

remote='origin'
verbose=0
cmd=''

while [ -n "${1}" ]; do
    case "${1}" in
        '-v') verbose=$(( verbose + 1 ))    ;;
        '-r') remote="${2}"; shift          ;;
        *)    [ -z "${cmd}" ] && cmd="${1}" ;;
    esac

    shift
done


case "${cmd:-install}" in
    dependencies)
        git grep '^# *dep\(endency\)\?=' | cut -d'=' -f2- | tr ',' '\n' | sort | uniq
        ;;
    install)
        install && plugins
        ;;
    update)
        # shellcheck disable=SC2015
        update && { install && plugins; true; } || { plugins; false; }
        ;;
    *)
        printf 'Unrecognized command "%s"\n' "${cmd}" >&2
        exit 11
        ;;
esac
